""" From Wikipedia
	Start with a grid full of walls.
	Pick a cell, mark it as part of the maze.
	Add the walls of the cell to the wall list.
	While there are walls in the list:
		Pick a random wall from the list. We now have two cases: either there exists exactly one unvisited cell on one of the two sides of the chosen wall, or there does not. If it is the former:
			Make the wall a passage and mark the unvisited cell as part of the maze.
			Add the neighboring walls of the cell to the wall list.
		Remove the wall from the list.
"""
import sys, termios, fcntl, os, copy, random
def getInput():
	fd = sys.stdin.fileno()

	oldterm = termios.tcgetattr(fd)
	newattr = termios.tcgetattr(fd)
	newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
	termios.tcsetattr(fd, termios.TCSANOW, newattr)

	oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
	fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)

	try:
		while 1:
			try:
				key = sys.stdin.read(1)
				if key == "w" or "a" or "s" or "d" or "p" or "z" or "x":
					break
			except IOError: pass
	finally:
		termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
		fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
	return key

def mazeGen(size):
	# Start with a grid full of walls
	size = (size*2)-1
	grid = range(size)
	for x in grid:
		grid[x] = []
		for y in range(size):
			grid[x].insert(y,True)
	size = size-1
	# Pick a cell, mark it as part of the maze.
	grid[0][0] = False

	# Add the walls of the cell to the wall list.
	wallList = [[0,1],[1,0]]

	# While there are walls in the list:
	if True:
		while len(wallList) != 0:
			# Pick a random wall from the list. And remove it from the list.
			wall = wallList.pop(random.choice(range(len(wallList))))
			# We now have two cases: Two cells or 1
			if wall[0]%2 == 0:
				if wall[1] != size:
					if grid[wall[0]][wall[1]+1]:
						# Make the wall and cell part of the maze.
						grid[wall[0]][wall[1]+1] = False
						grid[wall[0]][wall[1]] = False
						# add the neighboring walls of the cell to the wall list.
						if wall[0] != 0 and wall[1] != size:
							wallList.append([wall[0]-1,wall[1]+1])
						if wall[0] != size and wall[1] != size:
							wallList.append([wall[0]+1,wall[1]+1])
						if wall[1] != size-1:
							wallList.append([wall[0],wall[1]+2])
				if wall[1] != 0:
					if grid[wall[0]][wall[1]-1]:
						# Make the wall and cell part of the maze.
						grid[wall[0]][wall[1]-1] = False
						grid[wall[0]][wall[1]] = False
						# add the neighboring walls of the cell to the wall list.
						if wall[0] != 0 and wall[1] != 0:
							wallList.append([wall[0]-1,wall[1]-1])
						if wall[0] != size and wall[1] != 0:
							wallList.append([wall[0]+1,wall[1]-1])
						if wall[1] != 1:
							wallList.append([wall[0],wall[1]-2])
			else:
				if wall[0] != size:
					if grid[wall[0]+1][wall[1]]:
						# Make the wall and cell part of the maze.
						grid[wall[0]+1][wall[1]] = False
						grid[wall[0]][wall[1]] = False
						# add the neighboring walls of the cell to the wall list.
						if wall[1] != 0 and wall[0] != size:
							wallList.append([wall[0]+1,wall[1]-1])
						if wall[1] != size and wall[0] != size:
							wallList.append([wall[0]+1,wall[1]+1])
						if wall[0] != size-1:
							wallList.append([wall[0]+2,wall[1]])
				if wall[0] != 0:
					if grid[wall[0]-1][wall[1]]:
						# Make the wall and cell part of the maze.
						grid[wall[0]-1][wall[1]] = False
						grid[wall[0]][wall[1]] = False
						# add the neighboring walls of the cell to the wall list.
						if wall[0] != 1:
							wallList.append([wall[0]-2,wall[1]])
						if wall[1] != 0 and wall[0] != 0:
							wallList.append([wall[0]-1,wall[1]-1])
						if wall[1] != size and wall[0] != 0:
							wallList.append([wall[0]-1,wall[1]+1])
	return grid

maze = mazeGen(50)
playerX = 0
playerY = 0
while True:
	output = copy.deepcopy(maze)
	output[playerX][playerY] = "@"
	for x in output:
		for y in range(len(x)):
			if x[y] == True:
				x[y] = "#"
			elif not x[y]:
				x[y] = " "
	for x in range(len(output)):
		output[x] = ''.join(output[x])
	print '\n'.join(output)
	key = getInput()
	# Movement
	if key == "a":
		if playerY != 0 and not maze[playerX][playerY-1]:
			playerY = playerY - 1
	if key == "w":
		if playerX != 0 and not maze[playerX-1][playerY]:
			playerX = playerX - 1
	if key == "d":
		if playerY != len(maze)-1 and not maze[playerX][playerY+1]:
			playerY = playerY + 1
	if key == "s":
		if playerX != len(maze)-1 and not maze[playerX+1][playerY]:
			playerX = playerX + 1
	# debug stuff
	if key == "x":
		break
	if playerX == len(maze)-1 and playerY == len(maze)-1:
		playerX = 0
		playerY = 0
		maze = mazeGen(50)
