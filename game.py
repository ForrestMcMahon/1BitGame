playerY = 0
playerX = 0

def refresh():
    screen[playerY][playerX] = "@"
    for x in range(hight):
        output = ""
        for y in range(width):
            output = output + screen[x][y]
        print output

def getInput():
    global key
    fd = sys.stdin.fileno()

    oldterm = termios.tcgetattr(fd)
    newattr = termios.tcgetattr(fd)
    newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
    termios.tcsetattr(fd, termios.TCSANOW, newattr)

    oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)

    try:
        while 1:
            try:
                key = sys.stdin.read(1)
                if key == "w" or "a" or "s" or "d" or "p" or "z" or "x":
                    break
            except IOError: pass
    finally:
        termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)

gameIsRunning = True
turn = 0
refresh()

while gameIsRunning:
    getInput()
    # Movement
    if key == "w":
        if playerY != 1:
            playerY = playerY - 1
    if key == "a":
        if playerX != 1:
            playerX = playerX - 1
    if key == "s":
        if playerY != hight-2:
            playerY = playerY + 1
    if key == "d":
        if playerX != width-2:
            playerX = playerX + 1
    # pause menu
    if key == "p":
        pauseMenu()
        getInput()
        key = ""
    # debug stuff
    if key == "x":
        gameIsRunning = False
    if key == "z":
        if debug == True:
            debug = False
        else:
            debug = True
    if debug:
        print "Turn:" + str(turn)
        print "Key:" + key
        print "X:" + str(playerX)
        print "Y:" + str(playerY)
    refresh()
    turn += 1
